resource "aws_s3_bucket" "job_offers" {
  bucket = "nom-du-bucket-unique-ilyes-diny-tp2-serverless-backend"
}

resource "aws_s3_object" "job_offers_directory" {
  bucket = aws_s3_bucket.job_offers.bucket
  key    = "job_offers/"
}

resource "aws_s3_bucket_notification" "lambda_notification" {
  bucket = aws_s3_bucket.job_offers.bucket

  lambda_function {
    lambda_function_arn = aws_lambda_function.s3_to_sqs_lambda.arn
    events             = ["s3:ObjectCreated:*"]
    filter_prefix      = "job_offers/"
    filter_suffix      = ".csv"
  }

  depends_on = [aws_lambda_function.s3_to_sqs_lambda]
}
