resource "aws_sqs_queue" "job_offers_queue" {
  name                      = "job-offers-queue"
  delay_seconds             = 0
  max_message_size          = 256000
  message_retention_seconds = 86400
  receive_wait_time_seconds = 10

  redrive_policy = jsonencode({
    deadLetterTargetArn = aws_sqs_queue.deadletter_queue.arn,
    maxReceiveCount     = 4
  })
}

resource "aws_sqs_queue" "deadletter_queue" {
  name = "deadletter-queue"
}

resource "aws_lambda_event_source_mapping" "sqs_to_dynamo_lambda_mapping" {
  event_source_arn = aws_sqs_queue.job_offers_queue.arn
  function_name    = aws_lambda_function.sqs_to_dynamo_lambda.function_name
}

