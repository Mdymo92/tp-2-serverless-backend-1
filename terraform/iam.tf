resource "aws_iam_role" "s3_to_sqs_lambda_role" {
  name             = "s3_to_sqs_lambda_role"
  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = "sts:AssumeRole",
        Effect = "Allow",
        Principal = {
          Service = "lambda.amazonaws.com"
        }
      }
    ]
  })

  inline_policy {
    name = "s3_access_policy"

    policy = jsonencode({
      Version = "2012-10-17",
      Statement = [
        {
          Effect   = "Allow",
          Action   = ["s3:PutObject", "s3:GetObject"],
          Resource = "${aws_s3_bucket.job_offers.arn}/*"
        }
      ]
    })
  }
}

resource "aws_iam_role" "sqs_to_dynamo_lambda_role" {
  name             = "sqs_to_dynamo_lambda_role"
  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = "sts:AssumeRole",
        Effect = "Allow",
        Principal = {
          Service = "lambda.amazonaws.com"
        }
      }
    ]
  })

  inline_policy {
    name = "sqs_to_dynamo_sqs_policy"

    policy = jsonencode({
      Version = "2012-10-17",
      Statement = [
        {
          Effect   = "Allow",
          Action   = ["sqs:ReceiveMessage", "sqs:DeleteMessage", "sqs:GetQueueAttributes"],
          Resource = aws_sqs_queue.job_offers_queue.arn
        }
      ]
    })
  }

  inline_policy {
    name = "sqs_to_dynamo_dynamodb_policy"

    policy = jsonencode({
      Version = "2012-10-17",
      Statement = [
        {
          Effect   = "Allow",
          Action   = "dynamodb:PutItem",
          Resource = aws_dynamodb_table.job_table.arn
        }
      ]
    })
  }
}

resource "aws_iam_role" "job_api_lambda_role" {
  name = "job_api_lambda_role"
  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect = "Allow",
        Action = "sts:AssumeRole",
        Principal = {
          Service = "lambda.amazonaws.com"
        }
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "lambda_basic_execution" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaRole"
  role       = aws_iam_role.job_api_lambda_role.name
}

resource "aws_iam_role_policy" "job_api_lambda_policy" {
  name = "job_api_lambda_policy"
  role = aws_iam_role.job_api_lambda_role.id

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect   = "Allow",
        Action   = "dynamodb:*",
        Resource = aws_dynamodb_table.job_table.arn
      }
    ]
  })
}






resource "aws_iam_role_policy_attachment" "lambda_exec_attachment" {
  policy_arn = var.lambda_exec_policy_arn
  role       = aws_iam_role.job_api_lambda_role.name
}


