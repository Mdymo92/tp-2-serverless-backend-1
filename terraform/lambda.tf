resource "aws_cloudwatch_log_group" "lambda_log_group" {
  name = "/aws/lambda/s3_to_sqs_lambda"
  retention_in_days = 7
}

data "archive_file" "empty_zip_code_lambda" {
  type        = "zip"
  output_path = "${path.module}/empty_lambda_code.zip"
  source {
    content  = <<EOT
console.log("Hello, world!");
EOT
    filename = "index.js"
  }
}

resource "aws_lambda_function" "s3_to_sqs_lambda" {
  function_name    = "s3_to_sqs_lambda"
  handler          = "index.handler"
  memory_size      = 512
  timeout          = 900
  runtime          = "nodejs14.x"
  filename         = data.archive_file.empty_zip_code_lambda.output_path
  role             = aws_iam_role.s3_to_sqs_lambda_role.arn
  source_code_hash = filebase64sha256(data.archive_file.empty_zip_code_lambda.output_path)

  environment  {
    variables = {
      QUEUE_URL = aws_sqs_queue.job_offers_queue.id
    }
  }

  tracing_config {
    mode = "Active"
  }

  depends_on = [aws_cloudwatch_log_group.lambda_log_group]
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_lambda_permission" "allow_execution_from_s3" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.s3_to_sqs_lambda.function_name
  principal     = "s3.amazonaws.com"
  source_arn    = aws_s3_bucket.job_offers.arn
}

resource "aws_lambda_function" "sqs_to_dynamo_lambda" {
  function_name    = "sqs_to_dynamo_lambda"
  handler          = "index.handler"
  memory_size      = 512
  timeout          = 30
  runtime          = "nodejs14.x"
  filename         = data.archive_file.empty_zip_code_lambda.output_path
  role             = aws_iam_role.sqs_to_dynamo_lambda_role.arn
  source_code_hash = filebase64sha256(data.archive_file.empty_zip_code_lambda.output_path)

  environment  {
    variables = {
      TABLE_NAME = aws_dynamodb_table.job_table.name
    }
  }

  tracing_config {
    mode = "Active"
  }

  depends_on = [aws_cloudwatch_log_group.lambda_log_group]
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_lambda_permission" "allow_execution_from_sqs" {
  statement_id  = "AllowExecutionFromSQS"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.sqs_to_dynamo_lambda.function_name
  principal     = "sqs.amazonaws.com"
  source_arn    = aws_sqs_queue.job_offers_queue.arn
}

resource "aws_lambda_function" "job_api_lambda" {
  function_name    = "job_api_lambda"
  handler          = "index.handler"
  memory_size      = 512
  timeout          = 30
  runtime          = "nodejs14.x"
  filename         = data.archive_file.empty_zip_code_lambda.output_path
  role             = aws_iam_role.job_api_lambda_role.arn  # Référencez le rôle IAM ici
  source_code_hash = filebase64sha256(data.archive_file.empty_zip_code_lambda.output_path)

  environment {
    variables = {
      TABLE_NAME = aws_dynamodb_table.job_table.name
    }
  }

  tracing_config {
    mode = "Active"
  }

  depends_on = [aws_cloudwatch_log_group.lambda_log_group]
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_lambda_permission" "allow_execution_from_apigateway" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.job_api_lambda.function_name
  principal     = "apigateway.amazonaws.com"
  source_arn    = aws_apigatewayv2_api.job_api_gw.execution_arn  # Ajoutez cette ligne
}

